Long Polling Example
====================
*Tom Slankard*

This is a simplistic long-polling example using node.js + express.js + jquery.

The two important components are app.js and public/index.html. The former is the server, and the latter is the "client."

To run the demo, simply launch the server

    node app.js

This starts the server on port 3000 by default. Then, open a browser and navigate to

    http://localhost:3000/index.html

The page continually asks the server for a random number. The server satisfies the request after a random time interval between zero and five seconds. 

The page's AJAX request timeout is set to a value that is too short on purpose, so the polling request will occasionally time out. This is to demonstrate the use of the AJAX error handler.
